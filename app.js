// app.js

import {
  getUserInfo
} from "./api";

App({
  onLaunch() {

  },

  //用于判断是否存在登录信息
  checkLogin: function () {
    var token = this.globalData.token|| wx.getStorageSync('token');
    var userBaseInfo = this.globalData.userBaseInfo||wx.getStorageSync('userBaseInfo');
    var skey = this.globalData.skey||wx.getStorageSync('skey');
    if (token && userBaseInfo && skey) {
      this.globalData.userBaseInfo = userBaseInfo;
      this.globalData.token = token;
      this.globalData.skey = skey;
      return true;
    }
    return false;
  },
  //用于获取授权信息
  getSkey() {
    var skey = this.globalData.skey||wx.getStorageSync('skey');
    this.globalData.skey = skey;
    return skey;
  },
  //用于获取授权信息
  getWxUserInfo() {
    var userInfo = this.globalData.userInfo||wx.getStorageSync('userInfo');
    this.globalData.userInfo = userInfo;
    return userInfo;
  },
  //  记录用户是否授权
  userGlobalData: {
    hasAuthorized: false, // 初始值为未授权
  },
  //用于获取数据库基本个人信息
  getDbUserInfo() {
    var userBaseInfo = this.globalData.userBaseInfo ||wx.getStorageSync('userBaseInfo');
    this.globalData.userBaseInfo = userBaseInfo;
    return userBaseInfo;
  },

  //登录微信获取code并换取个人信息
  doLogin(notToPage) {
    wx.login({
      //成功放回
      success: (res) => {
        console.log(res);
        let code = res.code
        getUserInfo(code).then(res => {
          let data = res.data
          if (data.loginStatus == 1) {
            wx.setStorageSync('userBaseInfo', data.userInfo)
            wx.setStorageSync('token', data.userInfo.token)
            wx.setStorageSync('skey', data.skey)
            wx.switchTab({
              url: '/pages/index/index',
            })
          }
         else if(data.loginStatus == 3){
            wx.setStorageSync('skey', data.skey)
            if(notToPage==null||!notToPage){
              wx.navigateTo({
                url: '/pages/login/login',
              })
            }
          }
        })
      }
    })
  },
  






  globalData: {
    // 授权信息
    userInfo: null,
    // 用户基本信息
    userBaseInfo: null,
    // skey
    skey: null,
    // 本地
    // BASE_FILE_URL: "http://192.168.0.138:8022/yethan",
    // 网络
    BASE_FILE_URL: "https://civiltest.ix2.com/yethan",
     // 图片上传 
     BASE_IMG_URL: "http://192.168.0.107:9200/yethan/file/uploadOne", 
     // 图片回显地址 
     IMG_URL: "http://192.168.0.102:8888", 
  }
})