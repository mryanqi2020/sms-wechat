// pages/login/childrenPage/loginIndex.js
import {getLogin} from "../../../api.js";
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    active: 1,
    username:'', // 用户账号
    password:'', // 登录密码
  },

  onChangeusername(event) {
    this.setData({
      username:event.detail
    })
  },
  onChangepassword(event) {
    this.setData({
      password:event.detail
    })
  },
  loginUser(){
      let skey = app.getSkey();
      let {username,password} = this.data;
      getLogin({username,password,skey}).then(res=>{
        if(res.code=='00000'){
          wx.setStorageSync('skey',res.data.skey);
          wx.setStorageSync('token',res.data.userInfo.token);
          wx.setStorageSync('userBaseInfo',res.data.userInfo);
          wx.switchTab({
            url:"/pages/index/index",
          });
        }else{
          wx.showModal({
            title: '登录失败',
            content: res.message,
          })
        }
      });
  },



  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})