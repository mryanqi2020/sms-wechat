// pages/login/login.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },
// 跳转登录
  toLogin(e){
    wx.navigateTo({
      url: './childrenPage/loginIndex',
    })
    wx.setStorageSync('loginType', e.currentTarget.dataset.type)
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(app.globalData);
    // 检查用户是否已授权
    if (!getApp().globalData.hasAuthorized) {
    // 用户未授权，直接跳转到授权页面
    wx.navigateTo({
      url: '/pages/accredit/accredit',
    });
  }
    if (!app.checkLogin()) {
      app.doLogin(true);
    }else{
      wx.switchTab({
        url: '/pages/index/index',
      })
    }
  },
  

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})