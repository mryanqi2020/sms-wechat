// pages/student/qingjiashenqing/woyaoqingjia/woyaoqingjia.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    qingjialeixingshow: false,
    qingjialeixing: [
      {
        name: '病假',
      },
      {
        name: '事假',
      },
      {
        name: '丧假',
      }
    ],
    startdata:"",//开始日期
    kaishiriqiShow:false,
    kaishiriqi:[
      {}
    ]
  },
  // 请假类型
  onClose() {
    this.setData({ qingjialeixingshow: false });
  },
  onSelect(event) {
    console.log(event.detail);
  },
  qingjialeixing(){
    this.setData({
      qingjialeixingshow:true
    })
    },
  // 开始日期
  onDisplay(){
    this.setData({
      kaishiriqiShow:true
    })
  },
  onCloseStartTime(){
    this.setData({ kaishiriqiShow: false });
  },
  formatDate(date) {
    date = new Date(date);
    return `${date.getMonth() + 1}/${date.getDate()}`;
  },
  onConfirm(event){
    this.setData({
      kaishiriqiShow: false,
      startdata: this.formatDate(event.detail),
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})