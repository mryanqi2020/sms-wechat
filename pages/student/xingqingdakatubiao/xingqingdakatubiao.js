// pages/student/xingqingdakatubiao/xingqingdakatubiao.js
import {signAdd} from "../../../api"

Page({

  /**
   * 页面的初始数据
   */
  data: {
    iconMap: [
      {
        text:'美好一天',
        icon:'/images/image/xq-mhyt.png',
        moodType:"心情极佳",
        moodLevel: "1级",
      },
      {
        text:'元气满满',
        icon:'/images/image/xq-yqmm.png',
        moodType:"心情极佳",
        moodLevel: "1级",
      },
      {
        text:'精神倍加',
        icon:'/images/image/xq-jsjb.png',
        moodType:"心情极佳",
        moodLevel: "1级",
      },
      {
        text:'学习快乐',
        icon:'/images/image/xq-xxkl.png',
        moodType:"心情舒畅",
        moodLevel: "2级",
      },
      {
        text:'美丽心情',
        icon:'/images/image/xq-mlxq.png',
        moodType:"心情舒畅",
        moodLevel: "2级",
      },
      {
        text:'愉快生活',
        icon:'/images/image/xq-yksh.png',
        moodType:"心情舒畅",
        moodLevel: "2级",
      },
      {
        text:'学习搬砖',
        icon:'/images/image/xq-xxbz.png',
        moodType:"心情平和",
        moodLevel: "3级",
      },
      {
        text:'积极向上',
        icon:'/images/image/xq-jjxs.png',
        moodType:"心情平和",
        moodLevel: "3级",
      },
      {
        text:'努力学习',
        icon:'/images/image/xq-nlxx.png',
        moodType:"心情平和",
        moodLevel: "3级",
      },
      {
        text:'心情烦闷',
        icon:'/images/image/xq-xqfm.png',
        moodType:"心情较差",
        moodLevel: "4级",
      },
      {
        text:'需要关心',
        icon:'/images/image/xq-xygx.png',
        moodType:"心情较差",
        moodLevel: "4级",
      },
      {
        text:'没有兴趣',
        icon:'/images/image/xq-myxq.png',
        moodType:"心情较差",
        moodLevel: "4级",
      },
      {
        text:'总被伤害',
        icon:'/images/image/xq-zbsh.png',
        moodType:"心情极差",
        moodLevel: "5级",
      },
      {
        text:'极度郁闷',
        icon:'/images/image/xq-jdym.png',
        moodType:"心情极差",
        moodLevel: "5级",
      },
      {
        text:'心情低落',
        icon:'/images/image/xq-xqdl.png',
        moodType:"心情极差",
        moodLevel: "5级",
      },
    ],
    show:false,
    continuousDays:"15",//连续打卡天数
    curIndex:4,//-1表示当前没有选中项，可设置其他值，表示默认选中某个心情
    curXinqing:"",//当前心情
    moodType:"",//当前心情类别
    moodLevel:"",//当前心情等级
    clocked:false,//今日是否打卡   默认没打卡
  },
// 打卡今日心情
setCurXinqing(e){
  const index = e.currentTarget.dataset.index;
  const text = e.currentTarget.dataset.text;
  const moodlevel = e.currentTarget.dataset.moodlevel;
  const moodtype = e.currentTarget.dataset.moodtype;
  this.setData({
     curIndex: index,
     curXinqing: text,
     moodType:moodtype,
     moodLevel:moodlevel
    });
},
// 点击“选择”打卡-----弹出层
clockIn() {
  const parmas = {
    "studentId": "2023117306",
    "moodType": this.data.moodType,
    "moodLevel": this.data.moodLevel,
    "moodIntro":this.data.curXinqing
  }
  signAdd(parmas).then(res=>{
    console.log(res,"请求");
  })
  this.setData({ show: true });
},
// 关闭打卡弹出层
onClose() {
  this.setData({ show: false });
},
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})