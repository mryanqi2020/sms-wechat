// pages/student/xinqingdaka/xinqingdaka.js
import {
	getStudentMoodSign,
	getListByMonth,
	signAdd
} from "../../../api";
const app = getApp();
const module='/MoodSign'
Page({

	/**
	 * 页面的初始数据
	 */
	data: {

		pageType: '', // 页面类型
		pageTypeMap: {
			main: 'mainPage',
			detail: 'detailPage',
			edit: 'editPage'
		},
		show: true, // 日历显示
		dateString: "",
		xinqingPopup: false,
		curXinqing: '', // 当前心情
		imgUrl:'',
		messagePlaceholderVisible: true,
		moodHead: {},
		now: '',
		infoDate: '',
		moodIntro: '',
		iconMap: {
			'期待': '/images/image/xq-qd.png',
			'开心': '/images/image/xq-kx.png',
			'美滋滋': '/images/image/xq-mzz.png',
			'悠闲': '/images/image/xq-yx.png',
			'生气': '/images/image/xq-sq.png',
			'不开心': '/images/image/xq-bkx.png',
			'烦躁': '/images/image/xq-fz.png',
			'焦虑': '/images/image/xq-jv.png',
			'无聊': '/images/image/xq-wl.png',
			'难以描述': '/images/image/xq-nyms.png',
		},
		dakaData: [],
		xinqingType: {
			xi: {
				type: '喜',
				option: [{
						name: '期待',
						value: '期待',
						type: '喜',
					},
					{
						name: '开心',
						value: '开心',
						type: '喜'
					},
					{
						name: '美滋滋',
						value: '美滋滋',
						type: '喜'
					},
					{
						name: '悠闲',
						value: '悠闲',
						type: '喜'
					},
				]
			},
			sang: {
				type: '丧',
				option: [{
						name: '生气',
						value: '生气',
						type: '丧'
					},
					{
						name: '不开心',
						value: '不开心',
						type: '丧'
					},
					{
						name: '烦躁',
						value: '烦躁',
						type: '丧'
					},
					{
						name: '焦虑',
						value: '焦虑',
						type: '丧'
					},
				]
			},
			fuza: {
				type: '复杂',
				option: [{
						name: '无聊',
						value: '无聊',
						type: '复杂'
					},
					{
						name: '难以描述',
						value: '难以描述',
						type: '复杂'
					},
				]
			}
		},
		message: '',
		messageMaxLength: 140,
		fileList: [], // 文件上传列表
		// 图片数组
		images: []
	},
	dateChange(e) {
		this.setData({
			dateString: e.detail.dateString
		})
	},
	// 月份改变
	monthChange(e) {
		if (e.detail.month < 10) {
			e.detail.month = "0" + e.detail.month
		}
		getListByMonth({
			date: e.detail.year + "-" + e.detail.month + "-" + "01"
		}).then(res => {
			let map = this.data.dakaData.reduce((map, obj) => {
				map.set(obj.tid, obj);
				return map;
			}, new Map());
			const data = []
			res.data.map(e => {
				if (!map.get(e.tid)) {
					data.push(e)
				}
			})
			this.setData({
				dakaData: this.data.dakaData.concat(data)
			})
		})
	},
	// 设置当前心情
	setCurXinqing(e) {
   console.log(5);
		const {
			type
    } = e.currentTarget.dataset;
    console.log(e);
		this.setData({
			curXinqing: type
		})
	},
	// 打开选择心情弹窗
	// jiluxinqing(e) {
	// 	const {
	// 		type
  //   } = e.currentTarget.dataset;
    
	// 	this.setData({
	// 		xinqingPopup: true,
	// 		curXinqing: type || ''
	// 	})
  // },
  // 跳转到心情图标页面
jiluxinqing(){
  wx.navigateTo({
    url: '/pages/student/xingqingdakatubiao/xingqingdakatubiao',
  })
},
	// 取消选择心情
	cancelSelectXinqing() {
		this.setData({
			curXinqing: '',
			xinqingPopup: false,
		})
	},
	// 确认选择心情
	commitSelectXinqing() {
		this.setData({
			xinqingPopup: false,
			pageType: this.data.pageTypeMap.edit,
			imgUrl:''
		})
		this.messageBlur();
	},
	// 取消记录心情
	canclejiluxinqing() {
		this.setData({
			pageType: this.data.pageTypeMap.main,
			fileList: [],
			message: '',
		})
	},
	// 打卡今日心情
	save() {
		const param = {
			moodIntro: this.data.message,
			moodType: this.data.curXinqing,
			imgUrl:this.data.imgUrl
		}
		signAdd(param).then(res => {
			if (res.code == "00000") {
				wx.showToast({
					title: '打卡成功',
					icon: 'success',
					duration: 2000
				});
				var date = new Date()
				var year = date.getFullYear()
				var month = date.getMonth() + 1;
				this.monthChange({
					detail: {
						year: year,
						month: month
					}
        })
        getStudentMoodSign().then(res => {
          this.setData({
            moodHead: res.data
          })
        })
			}
			this.setData({
				pageType: this.data.pageTypeMap.main,
				fileList: [],
				message: '',
			})
		})
	},
	// 详情 转 编辑
	// 阿斯顿发撒法 -- action

	// 心情信息 获得焦点时
	messageFocus() {
		// 获取焦点时 message 无值 则  visible为 false, 有值则不设置
		const isWuZhi = this.data.message.length === 0;
		if (isWuZhi) {
			this.setData({
				messagePlaceholderVisible: false
			})
		}
	},
	// 心情信息 失去焦点时
	messageBlur() {
		this.setData({
			messagePlaceholderVisible: this.data.message.length === 0
		})
	},

	// 文件上传完毕 事件
	afterRead(event) {
		console.log(event, 111);
		const url = app.globalData.BASE_IMG_URL + module
		const {
			file
		} = event.detail;
		// 当设置 mutiple 为 true 时, file 为数组格式，否则为对象格式
		const list = []
		const that=this
		wx.uploadFile({
			url: url, // 仅为示例，非真实的接口地址
			filePath: file.url,
			header: {
				"Ytoken": wx.getStorageSync('token') || ''
			},
			name: 'file',
			formData: {
				user: 'test'
			},
			success(res) {
				if (res.data) {
					var obj = JSON.parse(res.data)
					if(obj){
						list.push({url:app.globalData.IMG_URL+obj.data.path})
						that.setData({
							fileList: list,
							imgUrl:obj.data.fileId
						});
						console.log(that.data.fileList);
					}
				}
				// 上传完成需要更新 fileList
				// const {
				// 	fileList = []
				// } = this.data;
				// fileList.push({
				// 	...file,
				// 	url: res.data
				// });
				// this.setData({
				// 	fileList
				// });
				
			},
		});
	},

	// 日历被点击
	calendarCellClick(e) {
		const {
			dateString,
			type
		} = e.detail;
		this.setData({
			moodIntro: "",
			infoDate: dateString,
			curXinqing: "",
			images:[]
		})
		const list=[]
		this.data.dakaData.map(e => {
			if (e.signDate == dateString) {
				if(e.imgUrl){
					list.push(app.globalData.IMG_URL+e.imgUrl)
				}
				this.setData({
					moodIntro: e.moodIntro||"",
					pageType: this.data.pageTypeMap.detail,
					curXinqing: type,
					now: dateString,
					images:list
				})
				console.log(this.data.images);
			}
		})
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		this.setData({
			pageType: this.data.pageTypeMap.main,
		})

	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
		var date = new Date()
		var year = date.getFullYear()
		var month = date.getMonth() + 1;
		var day = date.getDate();
		var formattedDate = year + '/' + month + '/' + day;
		this.setData({
			now: formattedDate
		})
		this.monthChange({
			detail: {
				year: year,
				month: month
			}
		})
		getStudentMoodSign().then(res => {
			this.setData({
				moodHead: res.data
			})
		})
	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	}
})