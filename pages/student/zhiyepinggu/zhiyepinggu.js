// pages/student/xinliceping/xinliceping.js
import { getAssessList } from "../../../api"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    active: 0,
    testData: [],
    allTestData: [],
    date: ''
  },
  getList() {
    getAssessList({ testType: '职业测评' }).then(res => {
      let allData = res.data || []
      let data = allData.filter(s => s.isFinished)
      this.setData({
        allTestData: allData || [],
        testData: data || [],
        date: (new Date()).getTime()
      })
    })
  },
  // 切换tab事件
  onChange(event) {
    // wx.showToast({
    //   title: `切换到标签 ${event.detail.name}`,
    //   icon: 'none',
    // });
  },
  // 查看结果
  chakanjieguo(e) {
    const { item } = e.currentTarget.dataset;
    // const {testType,result} = item
    // console.log(result);
    wx.removeStorageSync('itemResultData')
    wx.setStorageSync('itemResultData', item)
    wx.navigateTo({
      url: '/pages/student/zhiyepinggu/childrenPage/chakanjieguo',
    })
  },
  cepingqianxuzhi(e) {
    const { item } = e.currentTarget.dataset;
    var date =(new Date()).getTime()
    var start = (new Date(item.startTime)).getTime()
    var end = (new Date(item.endTime)).getTime()
    if (date >= start && date <= end) {
      if (!item.isFinished) {
        wx.removeStorageSync('testData')
        wx.setStorageSync('testData', item)
        wx.navigateTo({
          url: "/pages/student/zhiyepinggu/childrenPage/cepingqianxuzhi",
        })
      }
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getList()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  }
})