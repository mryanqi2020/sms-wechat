// pages/student/xinliceping/childrenPage/cepingshiti.js
import { getQuesList, addUserAnswer } from "../../../../api"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pageType: '',
    pageTypeMap: {
      dati: 'dati',
      tijiaofinish: 'tijiaofinish'
    },
    radio: '1',
    active: 0,  // 当前第几道题
    //  题目
    topics: [],
    // 提交的选项
    submitOptions: []
  },

  getQuesListInfo() {
    let tpid = wx.getStorageSync('templateData');
    getQuesList({ tpid }).then(res => {
      if (res.code = '00000') {
        this.setData({
          topics: res.data || []
        })
      }
    })
  },

  onClick(event) {
    const { name } = event.currentTarget.dataset;
    this.setData({
      radio: name,
    });
  },
  // 切换题目 - 监听
  onChangeTm(event) {
    this.setData({
      active: event.detail.name
    })
  },
  // 切换题目
  checkTopic(event) {
    const { type } = event.currentTarget.dataset;
    const [pre, next] = ['pre', 'next'];
    switch (type) {
      case pre:
        this.setData({ active: --this.data.active })
        break;
      case next:
        this.setData({ active: ++this.data.active })
        break;
    }
  },
  // 选择答案
  checkOptions(event) {
    // 获取当前选项 
    const { option, topicsitem } = event.currentTarget.dataset;

    const { quesType, orderNo, isCalc, isHelp, isJudge } = topicsitem;

    // 根据单选 多选 设置答案 
    let resOptions = this.data.submitOptions;

    const isQid = resOptions.find(d => d.qid === option.qid)

    if (!isQid) {
      // 不存在这道题（qid）id 的选项 - 直接存入当前选项
      resOptions.push({
        ...option,
        quesType,
        orderNo,
        isCalc,
        isHelp,
        isJudge
      })
    }

    if (quesType == '单选') {
      // 判断resOptions中是否已经存在这道题（qid）的选项（optionId）

      if (isQid) {
        const isOptionId = resOptions.find(d => d.optionId === option.optionId)
        if (!isOptionId) {
          // 不存在当前选项则存入当前选项 - 并将其他选项删除
          const otherOptionIdx = resOptions.findIndex(d => d.qid === option.qid);
          resOptions.splice(otherOptionIdx, 1, {
            ...option,
            quesType,
            orderNo,
            isCalc,
            isHelp,
            isJudge
          })
        } else {
          // 存在不做处理
        }
      }
    } else if (quesType == '多选') {
      // 判断resOptions中是否已经存在这道题（qid）的选项（optionId）
      if (isQid) {
        const isOptionId = resOptions.find(d => d.optionId === option.optionId)
        if (!isOptionId) {
          // 不存在当前选项则存入当前选项
          resOptions.push({
            ...option,
            quesType,
            orderNo,
            isCalc,
            isHelp,
            isJudge
          })
        } else {
          // 存在则取消选择当前选项
          const curOptionIdx = resOptions.findIndex(d => d.optionId === option.optionId);
          resOptions.splice(curOptionIdx, 1)
        }
      }
    }
    this.setData({
      submitOptions: resOptions
    })
  },

  // 提交选择的选项
  submit() {
    let testBean = wx.getStorageSync('testData');
    let answerOptionList = this.data.submitOptions
    addUserAnswer({ testBean, answerOptionList }).then(res=>{
      wx.navigateBack()
    })
    // this.setData({
    //   pageType: this.data.pageTypeMap.tijiaofinish
    // })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      pageType: this.data.pageTypeMap.dati
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getQuesListInfo()
    if (this.data.pageType === 'tijiaofinish') {
      setTimeout(() => {
        console.log(11111);
        wx.navigateTo({
          url: "pages/student/zhiyepinggu/zhiyepinggu",
        })
      }, 3000);
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})