// pages/student/xinliceping/childrenPage/chakanjieguo.js
import { testResult, reDoTest } from "../../../../api"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    testType: '',
    result: {},
    resultStr: [],
  },

  getResult() {
    let item = wx.getStorageSync('itemResultData');
    if (item) {
        testResult({ result: item.result }).then(res => {
          this.setData({
            testType: item.testType,
            result: res
          })
          console.log(this.data);
        })
    }
  },
  chengxinceping() {
    let item = wx.getStorageSync('itemResultData');
    var date = (new Date()).getTime()
    var start = (new Date(item.startTime)).getTime()
    var end = (new Date(item.endTime)).getTime()
    if ((date >=start  && date <= end) || !item.startTime) {
      wx.showModal({
        title: '提示',
        content: '确定要重新测评吗？',
        success: function (sm) {
          if (sm.confirm) {
            wx.removeStorageSync('testData');
            wx.setStorageSync('testData', item)
            let { tid, tpid } = item
            reDoTest({ tid, tpid }).then(res => {
              if (res.code == '00000') {
                wx.navigateTo({
                  url: "/pages/student/xinliceping/childrenPage/cepingqianxuzhi",
                })
              }
            })
          }
        }
      })
    } else {
      wx.showModal({
        title: '提示',
        content: '该测评已截止，无法重做！',
        success: function (sm) {
        }
      })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getResult()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})