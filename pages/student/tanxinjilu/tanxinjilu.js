// pages/student/tanxinjilu/tanxinjilu.js
import {
  talkRecordStudent
} from "../../../api.js";
import {
  getDateTime
} from "../../../utils/util.js";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userRole:"", // 用户角色		
    talkList:[]
  },
  getList(){
    let passId=wx.getStorageSync('userBaseInfo').passId
    talkRecordStudent({passId:passId}).then(res=>{
      let data=res.data||[]
      if(data){
        data.forEach(e=>{
          e.startTime=getDateTime(e.startTime,1)
          e.endTime=getDateTime(e.endTime,1)
          e.createTime=getDateTime(e.createTime,1)
        })
      }
      this.setData({
        talkList:data||[]
      })
    })
  },
  // 查看详情
  showDetail(e){
    let tid=e.currentTarget.dataset.tid
    let url='./childrenPage/details?tid='+tid
      wx.navigateTo({
        url: url, // action...
      })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getList()
    this.setData({
      userRole:wx.getStorageSync('userBaseInfo').userType
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})