import {
  getGoalList,
  getCurrentTerm,
  updateFinish
} from "../../../../api";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    params: {
      termId: ''
    },
    goalList: [],
    ifTerm: '',

  },
  getCreat() {
    let { goalList } = this.data
    if (goalList.length < 5) {
      wx.navigateTo({
        url: '../personPlan-creat/personPlan-creat',
      })
    } else {
      wx.showModal({
        title: '提示',
        content: '当前学期阶段目标已达到最大目标数，无需再进行添加',
        success: function (sm) {
        }
      })
    }
  },
  getList() {
    const { params } = this.data
    getGoalList(params).then(res => {
      let data = res.data
      this.setData({
        goalList: data
      })
    })
  },
  onChange(event) {
    let finish = event.detail
    let { tid, termId } = event.currentTarget.dataset.item
    updateFinish({ finish, tid, termId }).then(res => {
      if (res.code == 500) {
        wx.showModal({
          title: '提示',
          content: res.message,
          success: function (sm) {
          }
        })
      }
      this.getList()
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    const { params } = this.data
    params.termId = options.termId
    getCurrentTerm().then(res => {
      if (options.termId == res.termId) {
        this.setData({
          ifTerm: true
        })
      }
      else {
        this.setData({
          ifTerm: false
        })
      }
    })
    this.getList()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    let termId = wx.getStorageSync('currentTermId')
    const { params } = this.data
    params.termId = termId
    getCurrentTerm().then(res => {
      if (termId == res.termId) {
        this.setData({
          ifTerm: true
        })
      }
      else {
        this.setData({
          ifTerm: false
        })
      }
    })
    this.getList()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    wx.navigateTo({
      url: '../personPlan',
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
})