import {
  getPlan,
  getTermList,
  getCurrentTerm
} from "../../../api";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    option1: [
    ],
    termId:'',
    value: 100,
    titleArr: [],
    params: '',
    allTestData: [],
    passId: wx.getStorageSync('userBaseInfo').passId,
  },
  getInfo(e) {
    let termId = e.currentTarget.dataset.termid
    wx.removeStorageSync('currentTermId')
    wx.setStorageSync('currentTermId', termId)
    wx.navigateTo({
      url: '../personPlan/personPlan-info/personPlan-info',
    })
  },
  getList(params) {
    getPlan({ termId: params }).then(res => {
      let data = res.data.goal
      let allData = res.data.plan
      this.setData({
        titleArr: data,
        allTestData: allData || [],
        // testData: data || [],
        date: (new Date()).getTime()
      })
    })
  },
  wenjuanqianxuzhi(e) {
    const { item } = e.currentTarget.dataset;
    var date = (new Date()).getTime()
    var start = (new Date(item.startTime)).getTime()
    var end = (new Date(item.endTime)).getTime()
    if (date >= start && date <= end) {
      if (!item.isFinished) {
        wx.removeStorageSync('testData')
        wx.setStorageSync('testData', item)
        console.log(1);
        wx.navigateTo({
          url: "../personPlan/personPlan-rx/cepingqianxuzhi",
        })
      }
    }
  },
  // getRXList() {
  //   getAssessList({ testType: '入学测评' }).then(res => {
  //     let allData = res.data || []
  //     let data = allData.filter(s => s.isFinished)
  //     this.setData({
  //       allTestData: allData || [],
  //       testData: data || [],
  //       date: (new Date()).getTime()
  //     })
  //   })
  // },

  /**
   * 生命周期函数--监听页面加载
   */
  change(event) {
    this.setData({
      value: event.detail
    })
    this.getList(event.detail)
  },
  onLoad(options) {
    // const {params,passId} = this.data
    // this.getList()
    // this.getRXList()
    // getTermList({passId}).then(res=>{
    //   this.setData({
    //     option1 : res.map(d=>({...d,text:d.name}))
    //   })
    // })
  },
  getCreat() {
    let {termId,titleArr} = this.data
    let goal = titleArr.find(s=>s.termId==termId)
    if(!goal || goal.number<5){
      wx.navigateTo({
        url: '../personPlan/personPlan-creat/personPlan-creat',
      })
    }else{
      wx.showModal({
        title: '提示',
        content: '当前学期阶段目标已达到最大目标数，无需再进行添加',
        success: function (sm) {
        }
      })
    }
    
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    getCurrentTerm().then(res => {
      this.setData({
        termId:res.termId || '',
        value:res.termId || '',
      })
    })
    const {passId } = this.data
    this.getList()
    // this.getRXList()
    getTermList({ passId }).then(res => {
      this.setData({
        option1: res.map(d => ({ ...d, text: d.name }))
      })
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})