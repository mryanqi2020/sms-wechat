// pages/student/personPlan/personPlanDetails/personPlanDetails.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    inputCount: 0
  },
  // 输入事件处理函数
  inputHandler: function (e) {
    const inputValue = e.detail.value;
    const inputCount = inputValue.length;
console.log(e,5);
    // 更新数据
    this.setData({
      inputCount: inputCount
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})