// pages/student/xinliceping/childrenPage/cepingqianxuzhi.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    collapse: true, //是否折起
    isKnow: false,
    test: {},
  },

  // 查看全部
  handlechakan() {
    this.setData({
      collapse: !this.data.collapse,
    })
  },
  // 切换是否知晓
  changKnow(e) {
    this.setData({
      isKnow: !this.data.isKnow,
    })
  },
  // 开始测评
  handleStart() {
    wx.redirectTo({
      url: '/pages/student/zhiyepinggu/childrenPage/cepingshiti',
    })
  },

  getTestInfo() {
    let test = wx.getStorageSync('testData');
    this.setData({
      test:test
    })
    wx.removeStorageSync('templateData')
    wx.setStorageSync('templateData', test.tpid)
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getTestInfo()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})