import {
  getGoalTitle,
  saveStageGoal
} from "../../../../api";
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    autosize:{
      minHeight:100
    },
    fileList:[],
    mubiaotext:'',
    finishNum:'',
    bztext:'',
    params : {
      memo:'',
      content:'',
      goalInfo:'',
      goalStage:'',
      termId:null,
      termTitle:'',
    }
  },

	// afterRead(event) {
	// 	const url = app.globalData.BASE_IMG_URL + "/web"
	// 	const {
	// 		file
	// 	} = event.detail;
	// 	// 当设置 mutiple 为 true 时, file 为数组格式，否则为对象格式
	// 	const list = []
  //   const that=this
  //   console.log(file);
	// 	wx.uploadFile({
	// 		url: url, // 仅为示例，非真实的接口地址
	// 		filePath: file.url,
	// 		header: {
	// 			"Ytoken": wx.getStorageSync('token') || ''
	// 		},
	// 		name: 'file',
	// 		formData: {
	// 			user: 'test'
	// 		},
	// 		success(res) {
	// 			if (res.data) {
	// 				var obj = JSON.parse(res.data)
	// 				if(obj){
	// 					list.push({url:app.globalData.IMG_URL+obj.data.path})
	// 					that.setData({
	// 						fileList: list,
	// 						imgUrl:obj.data.fileId
	// 					});
	// 				}
	// 			}
	// 		},
	// 	});
	// },
  // deleteImg(event){
  //   let index = event.detail.index;
  //   const { params,fileList } = this.data;
  //   fileList.splice(index,1)
  //   params.fileIds.splice(index,1)
  //   params.filePaths.splice(index,1)
  //   this.setData({
  //     fileList
  //   })
  // },

  onChange(event) {
  const {params} = this.data
  params.content = event.detail
    this.setData({
      params
    })
  },
  onChange1(event) {
    const {params} = this.data
    params.goalInfo = event.detail
      this.setData({
        params
      })
  },
  onChange2(event) {
      const {params} = this.data
      params.memo = event.detail
        this.setData({
          params
        })
      },
  postBtn(){
    const {params} = this.data
    saveStageGoal(params).then(res=>{
      wx.showModal({
        title: '提示',
        content:'添加成功',
        showCancel:false,
        success(res){
          if (res.confirm) {
            wx.navigateBack()
          }
        }
      })
    })
  },
  onLoad(options) {
    getGoalTitle().then(res=>{
      let data = res.data
      this.setData({
        params : data
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})