// pages/student/attendance/component/qiandaojiemian/qiandaojiemian.js
import Dialog from '@vant/weapp/dialog/dialog';
import {
  getSignInfo,
  saveSign,
} from "../../../../../api";
const app = getApp();
const module = '/MoodSign'
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    time: 30 * 60 * 60 * 1000,
    timeData: {},
    errorShow: false,
    signInfo: {},
    address: '',
    imgUrl: '',
  },

  lifetimes: {
    attached: function () {
      let date = wx.getStorageSync('signId')
      let passId = wx.getStorageSync('userBaseInfo').passId
      let signId = date.signId;
      let className = date.className;
      let that = this
      // 在组件实例进入页面节点树时执行
      that.getInfo({
        signId: signId,
        passId: passId,
        className: className
      })
    },
  },
  /**
   * 组件的方法列表
   */
  methods: {
    photoSign() {
      wx.chooseImage({
        count: 1, // 最多可以选择的图片数量
        sourceType: ['album', 'camera'], // 可选择的图片来源，相册或相机
        success: function (res) {
          var tempFilePaths = res.tempFilePaths; // 选择的图片的临时文件路径数组
          // 将选中的图片路径传递给上传图片的功能
           // 假设只选择了一张图片，因此取数组中的第一个路径
          console.log(tempFilePaths[0], 1111);
          const url = app.globalData.BASE_IMG_URL + module
          wx.uploadFile({
            url: url, // 仅为示例，非真实的接口地址
            filePath: tempFilePaths[0],
            header: {
              "Ytoken": wx.getStorageSync('token') || ''
            },
            name: 'file',
            formData: {
              user: 'test'
            },
            success(res) {
              if (res.data) {
                var obj = JSON.parse(res.data)
                console.log(obj);
              }
            },
          });
        }
      });
    },
    onChange(e) {
      this.setData({
        timeData: e.detail,
      });
    },
    // 点击签到
    clickQiandao() {
      // 如果签到失败
      this.setData({
        errorShow: true
      })
    },
    // 关闭弹窗
    dialogClose() {
      const dialog = this.selectComponent("#my-dialog");
      // 手动关闭对话框
      dialog.close();
    },
    //获取位置
    setLocation() {
      let address = {}
      wx.getLocation({
        type: 'wgs84',
        success(res) {
          const latitude = res.latitude
          const longitude = res.longitude
          wx.openLocation({
            latitude: latitude,
            longitude: longitude,
            address: address,
          })
        }
      })
      this.setData({
        address: address
      })
    },
    //获取签到信息
    getInfo(e) {
      getSignInfo(e).then(res => {
        let data = res.data
        let status
        let time
        if (new Date(data.endTime).getTime > new Date().getTime && new Date(data.startTime).getTime <= new Date().getTime) {
          status = 1
          time = new Date(data.endTime).getTime - new Date.getTime
        } else if (new Date().getTime < new Date(data.startTime).getTime) {
          status = 2
        } else {
          status = 0
        }
        data.status = status
        data.className = e.className
        this.setData({
          signInfo: data,
          time: time || 0
        })
      })
    },
    save() {
      let data = this.data.signInfo
      data.address = this.data.address
      // saveSign(data).
    },

  }
})