// pages/student/attendance/component/putong/putong.js
import {
  getSignList
} from "../../../../../api";
import {
  getDateTime
} from "../../../../../utils/util.js";
Component({
  options: {
    styleIsolation: "apply-shared"
  },
  /**
   * 组件的属性列表
   */
  properties: {
   
  },

  /**
   * 组件的初始数据
   */
  data: {
    signList:[]
  },
  lifetimes:{
    attached:function () {
      let that=this
      that.getList()
    }
  },
  
  /**
   * 组件的方法列表
   */
  methods: {
    getList(){
      let parmas={
        passId:wx.getStorageSync('userBaseInfo').passId,
        checkType:'普通签到'
      }
      getSignList(parmas).then(res=>{
        let data=res.data||[]
        data.forEach(e => {
          let status
          if(new Date(e.endTime).getTime>new Date().getTime&&new Date(e.startTime).getTime<=new Date().getTime){
            status=1
          }else if(new Date().getTime<new Date(e.startTime).getTime){
            status = 2
          }else{
            status =0
          }
          e.status=status
          e.startTime=getDateTime(e.startTime)
          e.endTime=getDateTime(e.endTime)
        });
        this.setData({
          signList:data
        })
      })
    },
    toSignInfo(e){
      let {signId,className} = e.currentTarget.dataset.sign;
      wx.setStorageSync('signId', {signId,className})
      let url = '/pages/student/attendance/component/qiandaojiemian/qiandaojiemian';
      wx.navigateTo({
        url: url
      });
    }
  }
})
