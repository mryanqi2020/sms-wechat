// pages/student/attendance/component/quanbu/quanbu.js
const app = getApp()
import {
  getSignCount
} from "../../../../../api";
import {
  getDateTime
} from "../../../../../utils/util.js";
Component({
  options: {
    styleIsolation: "apply-shared"
  },
  /**
   * 组件的属性列表
   */
  properties: {
   
  },

  /**
   * 组件的初始数据
   */
  data: {
    dataCount:[]
  },
  lifetimes:{
    attached: function() {
      let that=this
      // 在组件实例进入页面节点树时执行
      that.getCount()
    },
  },
  /**
   * 组件的方法列表
   */
  methods: {
    getCount(){
      let passId=wx.getStorageSync('userBaseInfo').passId
      getSignCount({passId:passId}).then(res=>{
        let data=res.data
        if(data.signVoList){
          data.signVoList.forEach(e=>{
            e.startTime=getDateTime(e.startTime,1)
          })
        }else{
          data.signVoList=[]
        }
        this.setData({
          dataCount:data
        })
      })
    }
  }
})
