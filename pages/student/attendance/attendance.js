// pages/student/attendance/chaqingqiandao/chaqinqiandao.js
import {
  getSignCount
} from "../../../api";
Page({

  /**
   * 页面的初始数据
   */
  data: {
      userRole:"", // 用户角色		
      pagetype:'', // 页面类型
      // 页面类型映射
      pagetypeMap:{
         quanbu:'quanbu',
         putong:'putong',
         chaqin:'chaqin',
      },
      count:'',
      flag:false
  },

  // 切换页面类型
  checkpagetype(event){
    const {pagetype} = event.currentTarget.dataset;
    this.setData({pagetype:pagetype});
  },

 getCount(){
  let passId=wx.getStorageSync('userBaseInfo').passId
  getSignCount({passId:passId}).then(res=>{
    let data=res.data
    let count=''
    let flag=false
    if(data.signVoList){
      count=0
      data.signVoList.forEach(e=>{
        if((new Date(e.startTime).getTime()<new Date().getTime()&&new Date().getTime()<new Date(e.endTime).getTime())||new Date(e.startTime).getTime()>new Date().getTime()){
          count++
        }
      })
      if(count!=0){
        flag=true
      }
      count=data.signVoList.length
    }
    if(count>99){
      count='99+'
    }
    this.setData({
      count:count,
      flag:flag
    })
  })
 },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      // 初始化页面类型
      this.setData({pagetype:this.data.pagetypeMap.quanbu})
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getCount()
    this.setData({
      userRole:wx.getStorageSync('userBaseInfo').userType
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})