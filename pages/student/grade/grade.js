// pages/grade/grade.js
import {
	leaderList
} from "../../../api";
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
    userRole:"", // 用户角色		
    counselor: [],
		counselorAssistant: [],
		teacher: [],
		classLeader: [],
    member:[],
    active: 0,
    name:"哈哈",
    num:19950988563,
    options:"班长"

	},

	call(e) {
		const {
			phone
		} = e.currentTarget.dataset.phone;
		if (phone) {
			wx.makePhoneCall({
				phoneNumber: phone,
			})
		}
  },
  onChange(event) {
   console.log(11);
  },
  message(e) {
    const {
      name,passId
    } = e.currentTarget.dataset.item;
    wx.navigateTo({
      url: '/pages/teacher/grade/childrenPage/tongxunlu/childrenPage/message/sendMessage?name=' + name + "&passId=" + passId,
    })
  },
 
	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
  
	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
    const userType=wx.getStorageSync('userBaseInfo').userType
    if('student'==userType||'classleader'==userType){
      leaderList().then(res => {
        const list1 = []
        const list2 = []
        const list3 = []
        const list4 = []
        const list5 = []
        res.data.map(e => {
          if (e.role == "辅导员") {
            list1.push(e)
          } else if (e.role == "辅导员助理") {
            list2.push(e)
          } else if (e.role == "班级导师") {
            list3.push(e)
          } else if (e.role == "班干部") {
            list4.push(e)
          }else{
            list5.push(e)
          }
        })
        this.setData({
          counselor: list1,
          counselorAssistant: list2,
          teacher: list3,
          classLeader: list4,
          member:list5
        })
      })
    }
		
    this.setData({
      userRole:userType
    })
	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	}
})