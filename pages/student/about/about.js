// pages/about/about.js
import {getLogout} from '../../../api'
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: "",
    userTypeInfo:"",
    cellList: [{
      group: "基本信息",
      cententArr: [
        {
          imageUrl: "https://i03piccdn.sogoucdn.com/ad35119ab79cda25",
          title: "身份证号",
          value: ""
        },
        {
          imageUrl: "https://i03piccdn.sogoucdn.com/ad35119ab79cda25",
          title: "联系电话",
          value: ""
        },
        {
          imageUrl: "https://i03piccdn.sogoucdn.com/ad35119ab79cda25",
          title: "电子邮箱",
          value: ""
        },
        {
          imageUrl: "https://i03piccdn.sogoucdn.com/ad35119ab79cda25",
          title: "所属单位",
          value: ""
        },
      ]

    }]
  },
  // 注销
  logout(){

    wx.showModal({
      title: '提示',
      content: '解除绑定后需要重新绑定以继续使用，确认解除？',
      success: (result) => {
        if(result.confirm){
          getLogout().then(res=>{
            wx.redirectTo({
              url: '/pages/login/login',
            });
            //清除缓存
           wx.clearStorage();
           app.globalData.skey=null;
           app.globalData.token=null;
           app.globalData.userBaseInfo=null;
          })
        }
      },
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
   
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})