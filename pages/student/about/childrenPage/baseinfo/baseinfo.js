// pages/student/about/childrenPage/baseinfo/baseinfo.js
import{
  getUserStudent,
  changeRole
}from "../../../../../api.js"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo:{},
    roleTitle:'',
    show: false,
    actions: [],
  },
  getInfo(e){
    getUserStudent().then(res=>{
      let data = res.data
      console.log(data);
      this.setData({
        userInfo:data
      })
      this.showRole()
    })
  },
  openSheet() {
    this.onVisible(true)
  },
  sheetCancel() {
    this.onVisible(false)
  },
  // 角色动作面板显示隐藏
  onVisible(type) {
    this.setData({
      show: type
    });
  },
  //显示角色类别
  showRole(){
    let userBean=wx.getStorageSync('userBaseInfo')
    let roleTitle=userBean.roleTitle
    let roleMap = userBean.roleMap
    roleMap.forEach(e=>{
      e.name=e.roleTitle
    })
    this.setData({
      roleTitle:roleTitle,
      actions:roleMap
    })
  },
  // 角色动作面板选择 - 切换角色
  sheetSelect(e) {
    let other=e.detail
    changeRole(other).then(res=>{
      let data=res.data
      wx.setStorageSync('userBaseInfo', data)
      getApp().globalData.userBaseInfo=data
      this.onVisible(false)
      this.showRole()
    })
  },
  toNextUpdate1(){
    let tid=this.data.userInfo.tid
    wx.navigateTo({
      url: `/pages/student/about/childrenPage/baseinfo/updatePhone?tid=`+tid,
    })
  },
  toNextUpdate2(){
    let tid=this.data.userInfo.tid
    wx.navigateTo({
      url: `/pages/student/about/childrenPage/baseinfo/updateEmail?tid=`+tid,
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that=this
    that.getInfo()

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
      
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})