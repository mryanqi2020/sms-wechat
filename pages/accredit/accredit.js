// pages/accredit/accredit.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo:{},//用户信息
  },
  // 授权信息---获取用户头像，id等
  getUserInfo(){
    const _this = this
    wx.getUserProfile({
      desc: '获取用户的微信头像、ID等信息',
      success(res){
        // 用户授权成功，设置全局标志为已授权
        getApp().globalData.hasAuthorized = true;
        _this.setData({
          userInfo:res.userInfo
        })
        wx.navigateTo({
          url: '/pages/login/login',
        })
      },
      fail(err){
        console.log(err);
      }
    })

  },
  // 取消授权
  closeWx(){
    wx.navigateBack({
      delta: 1, // 返回上一个页面
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    console.log(getApp(),"2");
     // 检查用户是否已授权
     if (getApp().userGlobalData.hasAuthorized) {
      // 用户已授权，直接跳转到登录页面
      wx.navigateTo({
        url: '/pages/login/login',
      });
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})