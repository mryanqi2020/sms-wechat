// pages/index/index.js
import {getStuAffairTalkRecord,newsTop} from "../../api";
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userRole:"", // 用户角色
    searchVal: "",
    circular:true, //是否采用衔接滑动
    indicatorDots: true,  // 是否显示指示点
    autoplay: true,  // 是否自动播放
    interval: 5000,  // 图片切换时间间隔
    duration: 500,  // 滑动动画时长
    indicatorColor: '#bac9f3',
    indicatorActiveColor: '#ffffff',
    news:[],
    images: [  // 图片数组
      'https://tse2-mm.cn.bing.net/th/id/OIP-C.D5InVH4C0-xwMl_SBRuMNQAAAA?pid=ImgDet&rs=1',
      'https://pic1.zhimg.com/v2-fa6a4a705a490e5e542a9346d8f6e104_r.jpg',
      'https://ts1.cn.mm.bing.net/th/id/R-C.1a09a8619d41518100dee5bed9854972?rik=q1Cd2vlL7FSwWA&riu=http%3a%2f%2fwww.liuxuehr.com%2fgaoxiao%2fxnjtdx_emxq%2fimages%2fpic1.jpg&ehk=kv1tvHbMGHSS7En2fhlcChvVRWk%2b6Cu8cSIACePpkPM%3d&risl=&pid=ImgRaw&r=0',
    ],
    announcementList:[ //通知公告
      {
        img:"https://modao.cc/res-img/org/ppl/5.jpg",
        title:"关于对吴春林等2名学生退学处理",
        source:"来源：教务处",
        time:"时间：2023-11-06 16:21:15"
      },
      {
        img:"https://img.yzcdn.cn/vant/cat.jpeg",
        title:"2023-2024学年第一学期第7周教",
        source:"来源：教务处",
        time:"时间：2023-11-06 16:21:15"
      },
      {
        img:"https://modao.cc/res-img/org/ppl/5.jpg",
        title:"关于2022-2023学年第2学期补考",
        source:"来源：教务处",
        time:"时间：2023-11-06 16:21:15"
      },
    ]
  },
  
  getNewsTop(){
    newsTop().then(res=>{
      let result = res.data || []
      this.setData({
        news:result
      })
    })
  },
  // 搜索
  onSearch(e) {
 
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getNewsTop()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getNewsTop()
    this.setData({
      userRole:wx.getStorageSync('userBaseInfo')['userType'],
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})