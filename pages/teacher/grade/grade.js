// pages/teacher/grade/grade.js
import {
  classList
} from "../../../api";
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    selectType: '',
    selectTypeMap: {
      studentInfo: 'studentInfo',
      communication: 'communication'
    },
    phone: '',
    userName: '',
    roleTitle: '',
    tableData: []
  },
  onShow: function () {
    console.log(111);
  },


  /**
   * 组件的方法列表
   */
  methods: {
    // 进入班级列表
    intoList(e) {
      var classCode = e.currentTarget.dataset.code;

      if (this.data.selectType == this.data.selectTypeMap.studentInfo) {
        wx.navigateTo({
          url: '/pages/teacher/grade/childrenPage/classList/classList?classCode=' + classCode,
        })
      }else if(this.data.selectType == this.data.selectTypeMap.communication){
        wx.navigateTo({
          url: '/pages/teacher/grade/childrenPage/tongxunlu/tongxunlu?classCode=' + classCode,
        })
      }
    },
    // 类型选择
    selectTypeChange(event) {
      this.setData({
        selectType: event.detail
      })
    }
  },
  lifetimes: {
    attached() {


      const userInfo = wx.getStorageSync('userBaseInfo')

      this.setData({
        userName: userInfo.userName,
        phone: userInfo.mobilePhone,
        roleTitle: userInfo.roleTitle,
        selectType: this.data.selectTypeMap.studentInfo
      })
      classList({}).then(res => {
        if (res.code == "00000") {
          this.setData({
            tableData: res.data
          })
        }
      })
    }
  }
})