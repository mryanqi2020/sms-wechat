// pages/teacher/grade/childrenPage/classList/classList.js
import {
  classStudentList
} from "../../../../../api";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    active:0,
    tableData:[],
  },

  // 切换页面
  onChange(e){
    this.setData({
      active:e.detail.name
    })
  },
  intoInfo(e){
    const item = e.currentTarget.dataset.item;
    wx.removeStorageSync('studentDetail')
    wx.setStorageSync('studentDetail', item)
    wx.navigateTo({
      url: '/pages/teacher/grade/childrenPage/classList/studentDetails',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var pages = getCurrentPages(); // 获取当前页面栈的实例数组
    var currentPage = pages[pages.length - 1]; // 获取当前页面实例
    var classCode = currentPage.options.classCode; // 获取传递的参数值
    console.log(classCode);
    classStudentList({classCode:classCode}).then(res=>{
      if(res.code=="00000"){
        this.setData({
          tableData:res.data
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})