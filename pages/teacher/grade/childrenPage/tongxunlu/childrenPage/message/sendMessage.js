// pages/teacher/grade/childrenPage/tongxunlu/childrenPage/peopleControl/peopleControl.js
import {
  sendMessage
} from "../../../../../../../api";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    title: "",
    name:"",
    title:'',
    content:"",
    passId:''
  },
  handleInput(e) {
    console.log(e.detail.value); // 输出输入的文本
    this.setData({
      content:e.detail.value
    })
  },
  onChange(e){
    this.setData({
      title:e.detail
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  save(){
    var param={
      passId:this.data.passId,
      mailTitle:this.data.title,
      xxnr:this.data.content
    }
    sendMessage(param).then(res=>{
      if(res.code=="00000"){
        wx.showToast({
          title: '短信发送成功',
          icon: 'success',
          duration: 1500,
          success: function() {
            setTimeout(function() {
              wx.navigateBack({
                delta: 1
              });
            }, 1000);
          }
        })
      }else{
        wx.showToast({
          title: res.message,
          icon: 'none', 
        });
      }
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var pages = getCurrentPages(); // 获取当前页面栈的实例数组
    var currentPage = pages[pages.length - 1]; // 获取当前页面实例
    var name = currentPage.options.name; // 获取传递的参数值
    var passId = currentPage.options.passId;
    this.setData({
      name:name,
      passId:passId
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})