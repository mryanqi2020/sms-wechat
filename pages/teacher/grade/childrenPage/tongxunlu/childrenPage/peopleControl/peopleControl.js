// pages/teacher/grade/childrenPage/tongxunlu/childrenPage/peopleControl/peopleControl.js
import {
  classLeaderDuty,
  addClassLeader
} from "../../../../../../../api";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    dhhm: '',
    title: "",
    classCode: '',
    type: '',
    dutyList: [],
    selectedDuty: '',
    roleId: "",
    duty: "",
    name:"请选择"
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  save(){
    var param={
      passId:this.data.passId,
      roleId:this.data.roleId,
      duty:this.data.duty,
      classCode:this.data.classCode
    }
    addClassLeader(param).then(res=>{
      if(res.code=="00000"){
        wx.navigateTo({
          url: '/pages/teacher/grade/childrenPage/tongxunlu/tongxunlu?classCode=' + this.data.classCode
        })
      }else{
        wx.showToast({
          title: res.message,
          icon: 'none', 
        });
      }
    })
  },
  toChoose() {
    wx.navigateTo({
      url: '/pages/teacher/grade/childrenPage/tongxunlu/childrenPage/peopleControl/addClassLeader?type=' + this.data.type + "&classCode=" + this.data.classCode,
    })
  },
  dutyChange(e) {
    var value = e.detail.value
    this.data.dutyList.map(e => {
      if (e.value == value) {
        this.setData({
          roleId: value,
          duty: e.name
        })
      }
    })
    console.log(e);
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
    var pages = getCurrentPages(); // 获取当前页面栈的实例数组
    var currentPage = pages[pages.length - 1]; // 获取当前页面实例
    var type = currentPage.options.type; // 获取传递的参数值
    var classCode = currentPage.options.classCode;

    let date = wx.getStorageSync('classLeaderInfo')
    if (date) {
      this.setData({
        name:date.name,
        passId: date.passId
      })
      if(!type&&!classCode){
        type=date.type,
        classCode=date.classCode
      }
    }
    var temp = ''
    if (type == 1) {
      temp = "辅导员助理"
    } else if (type == 2) {
      temp = "班级导师"
    } else if (type == 3) {
      temp = "班干部"
      classLeaderDuty().then(res => {
        this.setData({
          dutyList: res.data
        })
      })
    }
    this.setData({
      title: temp,
      type: type,
      classCode: classCode
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})