// pages/teacher/grade/childrenPage/tongxunlu/childrenPage/peopleControl/peopleControl.js
import {
  studentListByCodeName
} from "../../../../../../../api";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    classCode: '',
    type: '',
    tableData: [],
  },
  onSearch(e) {
    console.log(e);
    studentListByCodeName({
      classCode: this.data.classCode,
      studentName: e.detail
    }).then(res => {
      if (res.code == "00000") {
        this.setData({
          tableData: res.data
        })
      }
    })
  },
  choose(e) {
    var item = e.currentTarget.dataset.item
    wx.removeStorageSync('classLeaderInfo')
    wx.setStorageSync('classLeaderInfo', {
      passId:item.studentId,
      name:item.studentName,
      type:this.data.type,
      classCode:this.data.classCode
    })
    wx.navigateBack({
      delta: 1, 
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var pages = getCurrentPages(); // 获取当前页面栈的实例数组
    var currentPage = pages[pages.length - 1]; // 获取当前页面实例
    var type = currentPage.options.type; // 获取传递的参数值
    var classCode = currentPage.options.classCode;
    this.setData({
      type: type,
      classCode: classCode
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})