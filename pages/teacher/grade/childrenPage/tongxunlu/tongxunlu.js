// pages/teacher/grade/childrenPage/tongxunlu/tongxunlu.js
import Dialog from '@vant/weapp/dialog/dialog';
import {
  leaderList
} from "../../../../../api";

Page({

  /**
   * 页面的初始数据
   */
  data: {
    counselorAssistant: [],
    teacher: [],
    classLeader: [],
    member: [],
    classCode: ''
  },

  // 删除
  commitDelete(event) {
    Dialog.confirm({
      message: '确定删除吗？',
    }).then(() => {

    });
  },
  // 添加
  addPeople(e) {
    const type = e.currentTarget.dataset.type;
    wx.navigateTo({
      url: '/pages/teacher/grade/childrenPage/tongxunlu/childrenPage/peopleControl/peopleControl?type=' + type + "&classCode=" + this.data.classCode,
    })
  },
  call(e) {
    const {
      phone
    } = e.currentTarget.dataset.phone;
    if (phone) {
      wx.makePhoneCall({
        phoneNumber: phone,
      })
    }
  },
  message(e) {
    const {
      name,passId
    } = e.currentTarget.dataset.item;
    wx.navigateTo({
      url: '/pages/teacher/grade/childrenPage/tongxunlu/childrenPage/message/sendMessage?name=' + name + "&passId=" + passId,
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.removeStorageSync('classLeaderInfo')
    var pages = getCurrentPages(); // 获取当前页面栈的实例数组
    var currentPage = pages[pages.length - 1]; // 获取当前页面实例
    var classCode = currentPage.options.classCode; // 获取传递的参数值
    this.setData({
      classCode: classCode
    })
    const list2 = []
    const list3 = []
    const list4 = []
    const list5 = []
    leaderList({
      classCode: classCode
    }).then(res => {
      if (res.code == "00000") {
        res.data.map(e => {
          if (e.role == "辅导员") {} else if (e.role == "辅导员助理") {
            list2.push(e)
          } else if (e.role == "班级导师") {
            list3.push(e)
          } else if (e.role == "班干部") {
            list4.push(e)
          } else {
            list5.push(e)
          }
        })
        this.setData({
          counselorAssistant: list2,
          teacher: list3,
          classLeader: list4,
          member: list5
        })
      }

    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})