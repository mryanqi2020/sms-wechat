// pages/teacher/tanxinjilu/tanxinjilu.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
     classValue:0,
     classOption:[
      { text: '2020机械-1', value: 0 },
      { text: '2020机械-2', value: 1 },
      { text: '2020机械-3', value: 2 }
     ]
  },

  /**
   * 组件的方法列表
   */
  methods: {
      // 新增
      add(){
        wx.navigateTo({
          url: '/pages/teacher/tanxinjilu/childrenPage/add/add',
        })
      },
      // 查看详情
      showDetail(){
        wx.navigateTo({
          url: '/pages/teacher/tanxinjilu/childrenPage/details/details',
        })
      }
  }
})
