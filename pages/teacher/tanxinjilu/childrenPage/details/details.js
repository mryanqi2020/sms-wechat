// pages/student/tanxinjilu/childrenPage/details.js
import {
  talkRecordStudent
} from "../../../../../api.js";
import {
  getDateTime
} from "../../../../../utils/util.js";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    collapse:true,
    talkInfo:{}
  },

  // 切换全部
  handlechakan(){
      this.setData({
        collapse:!this.data.collapse
      })
  },
  //获取谈心记录详情
  getInfo(tid){
    let passId=wx.getStorageSync('userBaseInfo').passId
    talkRecordStudent({passId:passId,tid:tid}).then(res=>{
      let data=res.data[0]
      data.startTime=getDateTime(data.startTime,1)
      data.endTime=getDateTime(data.endTime,1)
      data.createTime=getDateTime(data.createTime,1)
      console.log(data);
      this.setData({
        talkInfo:data
      })
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let tid=options.tid
    this.getInfo(tid)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})