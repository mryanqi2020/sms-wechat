// pages/teacher/attendance/childrenPage/faqiqiandao/faqiqiandao.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    txzt: '',
    qdrq: '2023-08-10',
    kssj: '00:23',
    jssj: '00:12',
    autosize: {
      minHeight: 100
    },
    // 学生姓名
    xsxmOptions:[
      {
        name: '选项',
      },
      {
        name: '选项',
      },
    ],
    // 谈心类型 - 列表
    txlxOptions: [{
        name: '选项tx',
      },
      {
        name: '选项',
      },
      {
        name: '选项',
        subname: '描述信息',
      },
    ],
    // 谈心方式 - 列表
    txfsOptions: [{
        name: '选项fs',
      },
      {
        name: '选项',
      },
      {
        name: '选项',
        subname: '描述信息',
      },
    ],
  },


  // 学生姓名 - 弹窗打开
  selectXsxm() {
    this.setData({
      xsxmShow: true
    })
  },

  // 学生姓名 - 弹窗关闭
  onCloseXsxm() {
    this.setData({
      xsxmShow: false
    });
  },
  // 学生姓名 - 选择
  onSelectXsxm(event) {
    console.log(event.detail);
  },

  // 谈心类型 - 弹窗打开
  selectTxlx() {
    this.setData({
      txlxShow: true
    })
  },

  // 谈心类型 - 弹窗关闭
  onCloseTxlx() {
    this.setData({
      txlxShow: false
    });
  },
  // 谈心类型 - 选择
  onSelectTxlx(event) {
    console.log(event.detail);
  },



  // 谈心方式 - 弹窗打开
  selectTxfs() {
    this.setData({
      txfsShow: true
    })
  },

  // 谈心方式 - 弹窗关闭
  onCloseTxfs() {
    this.setData({
      txfsShow: false
    });
  },
  // 谈心方式 - 选择
  onSelectTxfs(event) {
    console.log(event.detail);
  },


  // 开始时间改变
  bindPickerChangeStart(e) {
    this.setData({
      kssj: e.detail.value
    })
  },
  // 结束时间改变
  bindPickerChangeEnd(e) {
    this.setData({
      jssj: e.detail.value
    })
  },
  // 签到日期改变
  bindPickerChangeQdrq(e) {
    this.setData({
      qdrq: e.detail.value
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})