// pages/teacher/attendance/childrenPage/faqiqiandao/faqiqiandao.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    qiandaoName: '',
    qiandaobanji: [],
    qdrq: '2023-08-10',
    kssj: '00:23',
    jssj: '00:12',
    qiandaosetting: [],
    qiandaomiaoshu:'',
    qdlxShow: false,
    autosize:{
      minHeight:100
    },
    qdlxOptions: [{
        name: '选项',
      },
      {
        name: '选项',
      },
      {
        name: '选项',
        subname: '描述信息',
      },
    ],
    qiandaobanjiList: [{
        name: '机械-1',
        value: 1
      },
      {
        name: '机械-1',
        value: 2
      },
      {
        name: '机械-1',
        value: 3
      },
      {
        name: '机械-1',
        value: 4
      }
    ],
    qiandaosettingList: [{
        name: '是否要定位',
        value: 1
      },
      {
        name: '是否要拍照',
        value: 2
      }
    ]
  },


  // 签到类型 - 弹窗打开
  selectQdlx() {
    this.setData({
      qdlxShow: true
    })
  },

  // 签到类型 - 弹窗关闭
  onCloseQdlx() {
    this.setData({
      qdlxShow: false
    });
  },
  // 签到类型 - 选择
  onSelectQdlx(event) {
    console.log(event.detail);
  },

  // 签到班级 - 选择
  onBanJiChange(event) {
    this.setData({
      qiandaobanji: event.detail,
    });
  },
  // 签到设置 - 选择
  onSettingChange(event) {
    this.setData({
      qiandaosetting: event.detail,
    });
  },
  // 发放模式 - 选择
  onFfmsChange(event){
    this.setData({
      ffms: event.detail,
    });
  },

  bindPickerChangeStart(e) {
    this.setData({
      kssj: e.detail.value
    })
  },
  bindPickerChangeEnd(e) {
    this.setData({
      jssj: e.detail.value
    })
  },
  bindPickerChangeQdrq(e) {
    this.setData({
      qdrq: e.detail.value
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})