// pages/teacher/attendance/childrenPage/qiandaoDetail/qiandaoDetail.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pagetype:'', // 页面类型
    // 页面类型映射
    pagetypeMap:{
       putong:'putong',
       chaqin:'chaqin',
    }
  },

    // 切换页面类型
    checkpagetype(event){
      const {pagetype} = event.currentTarget.dataset;
      this.setData({pagetype:pagetype});
    },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      // 初始化页面类型
      this.setData({pagetype:this.data.pagetypeMap.chaqin})
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})