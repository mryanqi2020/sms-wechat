// const { getSignList } = require("/api.js")
// import {
//   getDateTime
// } from "/utils/util.js";

Component({
  options: {
    styleIsolation: "apply-shared"
  },
  /**
   * 组件的属性列表
   */
  properties: {
   
  },

  /**
   * 组件的初始数据
   */
  data: {
    signList:[]
  },
  lifetimes:{
    attached: function() {
      let that=this
      // 在组件实例进入页面节点树时执行
      that.getList()
    },
  },
  /**
   * 组件的方法列表
   */
  methods: {
    getList(){
      let parmas={
        passId:wx.getStorageSync('userBaseInfo').passId,
        checkType:'查寝签到'
      }
      getSignList(parmas).then(res=>{
        let data=res.data||[]
        data.forEach(e=>{
          let status
          if(new Date(e.endTime).getTime>new Date().getTime&&new Date(e.startTime).getTime<=new Date().getTime){
            status=1
          }else if(new Date().getTime<new Date(e.startTime).getTime){
            status = 2
          }else{
            status =0
          }
          e.status=status
          e.startTime=getDateTime(e.startTime)
          e.endTime=getDateTime(e.endTime)
        })
        this.setData({
          signList:data
        })
      })
    }
  }
})
