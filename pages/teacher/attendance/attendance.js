import * as echarts from '../../../components/ec-canvas/echarts';

// 初始化图表
function initChart(canvas, width, height, dpr) {
  const chart = echarts.init(canvas, null, {
    width: width,
    height: height,
    devicePixelRatio: dpr // new
  });
  canvas.setChart(chart);

  var option = {
    backgroundColor: "#ffffff",
    series: [
      {
        type: 'pie',
        radius: ['44%', '80%'],
        label: {
          position: 'inside',
          formatter: '{d}%',
          color:'#fff',
          fontWeight:'bold',
          rotate:'tangential',
        },
        itemStyle:['red'],
        data: [
          {value:10,itemStyle:{color:'#5454D8'},name:'签到率'},
          {value:3,itemStyle:{color:'#E44D44'},name:'未签到率'}
        ]
      }
    ]
  };

  chart.setOption(option);
  return chart;
}
function initChart2(canvas, width, height, dpr) {
  const chart = echarts.init(canvas, null, {
    width: width,
    height: height,
    devicePixelRatio: dpr // new
  });
  canvas.setChart(chart);

  var option = {
    backgroundColor: "#ffffff",
    grid: {
      // 设置网格的层级为 -1（较小的负值）
      z: -1
    },
    series: [
      {
        type: 'pie',
        radius: ['44%', '80%'],
        label: {
          position: 'inside',
          formatter: '{d}%',
          color:'#fff',
          fontWeight:'bold',
          rotate:'tangential',
        },
        itemStyle:['red'],
        data: [
          {value:15,itemStyle:{color:'#81B337'},name:'正常率'},
          {value:20,itemStyle:{color:'#E44D44'},name:'未归率'},
          {value:5,itemStyle:{color:'#FB950F'},name:'晚归率'},
        ]
      }
    ]
  };

  chart.setOption(option);
  return chart;
}

Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    xueqi: '1',
    xueqiOption: [
      {
        text: '全部学期',
        value: ''
      },
      {
        text: '2022-2023',
        value: '1'
      },
      {
        text: '2024-2025',
        value: '2'
      },
    ],
    xueqiNumOption:[
      {
        text: '全部学期',
        value: ''
      },
      {
        text: '第1学期',
        value: '1'
      },
      {
        text: '第2学期',
        value: '2'
      },
    ],
    // 图表
    ec: {
      onInit: initChart
    },
    ec2:{
      onInit: initChart2
    }
  },

  pageLifetimes:{

  }, 

  /**
   * 组件的方法列表
   */
  methods: {
      // 切换学期
      xueqiChange(e){
         this.setData({
           xueqi:e.detail
         })
      },
      // 查看详情
      toDetail(){
        wx.navigateTo({
          url: '/pages/teacher/attendance/childrenPage/qiandaoDetail/qiandaoDetail',
        })
      },
      // 进入发起签到页面
      launch(){
        console.log('adsf');
        wx.navigateTo({
          url: '/pages/teacher/attendance/childrenPage/faqiqiandao/faqiqiandao',
        })
      }
  }
})