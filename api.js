// 接口api
import {
  Request
} from "./utils/request";

/**使用code登陆*/
export const getUserInfo = (code) => {
  return Request("/wechatLogin/getUserInfo", 'get', {
    code: code
  });
};

/**账密*/
export const getLogin = (parmas) => {
  return Request("/wechatLogin/login", 'post', parmas);
};

/**登出*/
export const getLogout = (parmas) => {
  return Request("/wechatLogin/logout", 'post', parmas);
};

/**学生谈话记录*/
export const getStuAffairTalkRecord = (parmas) => {
  return Request("/student/stuAffairTalkRecord/studentList", 'get', parmas);
};
//获取学生心情信息
export const getStudentMoodSign = (params) => {
  return Request("/weChat/stuAffairMoodSign/getStudentMoodSign", 'get', params)
};
//获取学生某月心情打卡信息
export const getListByMonth = (params) => {
  return Request("/weChat/stuAffairMoodSign/getListByMonth", 'get', params)
};
//心情打卡
export const signAdd = (params) => {
  return Request("/weChat/stuAffairMoodSign/add", 'post', params)
};
//班级组织
export const leaderList = (params) => {
  return Request("/weChat/leader/list", 'get', params)
};
/**全部签到数据*/
export const getSignCount = (parmas) => {
  return Request("/weChat/signCount", 'get', parmas);
};

//签到数据
export const getSignList = (parmas) => {
  return Request("/weChat/signList", 'get', parmas);
};

//签到数据详情
export const getSignInfo = (parmas) => {
  return Request("/weChat/signInfo", 'get', parmas);
};

//保存签到数据
export const saveSign = (parmas) => {
  return Request("/weChat/signSave", 'post', parmas);
};

/**用户心理测评信息*/
export const getUserTestMind = () => {
  return Request("/weChat/mind/assess", 'get');
}

/**用户测评信息*/
export const getUserTest = (params) => {
  return Request("/weChat/other/assess/testType", 'get', params);
}

/**测评题目信息获取 */
export const getQuesList = (params) => {
  return Request("/weChat/quesList/tpid", 'get', params);
}

/**测评答案提交 */
export const addUserAnswer = (params) => {
  return Request("/weChat/add/answer", 'post', params);
}

/**测评结果查看 */
export const testResult = (params) => {
  return Request("/weChat/result/info", 'get', params);
}

/**测评重做 */
export const reDoTest = (params) => {
  let { tpid, tid } = params
  return Request(`/weChat/deleteById/${tpid}/${tid}`, 'delete');
}

//切换用户角色
export const changeRole = (params) => {
  let { roleId } = params
  return Request(`/userAccount/changeRole/${roleId}`, 'get');
}
//获取用户信息（学生）
export const getUserStudent = (params) => {
  return Request(`/weChat/studentInfo`, 'get', params);
}
//修改邮箱/联系方式
export const updateStudentUser = (params) => {
  return Request(`/weChat/updateStudent`, 'post', params);
}
/**学生谈心记录（学生） */
export const talkRecordStudent = (params) => {
  return Request(`/weChat/talkRecordStudent`, 'get', params);
}
// 测评获取
export const getAssessList = (params) => {
  return Request(`/weChat/assess`, 'get', params);
}

// 目标标题获取
export const getGoalTitle = () => {
  return Request(`/weChat/goal/title`, 'get');
}

// 目标保存
export const saveStageGoal = (params) => {
  return Request(`/weChat/save/goal`, 'post', params);
}

// 目标获取
export const getGoalList = (params) => {
  return Request(`/weChat/goalList`, 'get', params);
}

// 个人规划
export const getPlan = (params) => {
  return Request(`/weChat/plan`, 'get',params);
}
/**辅导员班级信息 */
export const classList = (params) => {
  return Request(`/weChat/counselor/classList`, 'get',params);
}
/**班级成员信息 */
export const classStudentList = (params) => {
  return Request(`/weChat/counselor/classStudentList`, 'get',params);
}
/**获取当前学期*/
export const getCurrentTerm = () => {
  return Request(`/public/query/termId`, 'get');
}
/**获取当前学期*/
export const getTermList = (params) => {
  return Request(`/public/query/grade/term/list`, 'get',params);
}

/**班干部职位 */
export const classLeaderDuty = (params) => {
  return Request(`/weChat/classLeader/duty`, 'get',params);
}



/**班级学生列表 */
export const studentListByCodeName = (params) => {
  return Request(`/weChat/studentListByCodeName`, 'get',params);
}
/**添加班干部 */
export const addClassLeader = (params) => {
  return Request(`/weChat/addClassLeader`, 'post',params);
}
/**发送站内信 */
export const sendMessage = (params) => {
  return Request(`/weChat/sendMessage`, 'post',params);
}

/**目标进度更改 */
export const updateFinish = (params) => {
  return Request(`/weChat/updateFinish`, 'post',params);
}

/**公告 */
export const newsTop = () => {
  return Request(`/common/web/newsTop`, 'get');
}