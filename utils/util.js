
import barcode from './barcode';

/**  
 * 是否为Null  
 * @param object  
 * @returns {Boolean}  
 */
function isNull(object) {
  if (object == null || typeof object == "undefined") {
    return true;
  }
  return false;
};

/**  
 * 是否为为空  
 * @param object  
 * @returns {Boolean}  
 */
function isEmpty(object) {
  if (object == null || typeof object == "undefined" || object == '') {
    return true;
  }
  return false;
};

/** 
 * 根据日期字符串获取星期几 
 * @param dateString 日期字符串（如：2016-12-29），为空时为用户电脑当前日期 
 * @returns {String} 
 */
function getWeek(dateString) {
  var date;
  if (isNull(dateString)) {
    date = new Date();
  } else {
    var dateArray = dateString.split("-");
    date = new Date(dateArray[0], parseInt(dateArray[1] - 1), dateArray[2]);
  }
  //var weeks = new Array("日", "一", "二", "三", "四", "五", "六");  
  //return "星期" + weeks[date.getDay()];  
  return "星期" + "日一二三四五六".charAt(date.getDay());
};  

function formatTime(date) {
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()

  var hour = date.getHours()
  var minute = date.getMinutes()
  var second = date.getSeconds()


  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

function formatDateYY_MM_DD(date) {
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()
  return [year, month, day].map(formatNumber).join('-')
}

function formatTimeHH_MM(date) {
  var hour = date.getHours()
  var minute = date.getMinutes()

  return [hour, minute].map(formatNumber).join(':')
}

function formatNumber(n) {
  n = n.toString()
  return n[1] ? n : '0' + n
}

/** 
 * 根据开始日期结束日期获取之间的天数
 * @param strDateStart，strDateEnd 日期字符串（如：2016-12-29,2017-03-34），为空时为用户电脑当前日期 
 * @returns {int} 
 */
function getDays(strDateStart, strDateEnd) {
  var strSeparator = "-"; //日期分隔符
  var oDate1;
  var oDate2;
  var iDays;
  oDate1 = strDateStart.split(strSeparator);
  oDate2 = strDateEnd.split(strSeparator);
  var strDateS = new Date(oDate1[0], oDate1[1] - 1, oDate1[2]);
  var strDateE = new Date(oDate2[0], oDate2[1] - 1, oDate2[2]);
  iDays = parseInt(Math.abs(strDateS - strDateE) / 1000 / 60 / 60 / 24)//把相差的毫秒数转换为天数 
  return iDays;
}

//获取当前时间的周次
function getNowWeeks(strDateStart, strDateEnd) {
  var week = getWeek(strDateStart);
  var num2 = 0
  if (week == "星期一") {
    var num2 = 0
  } else if (week == "星期二") {
    var num2 = 1
  } else if (week == "星期三") {
    var num2 = 2
  } else if (week == "星期四") {
    var num2 = 3
  } else if (week == "星期五") {
    var num2 = 4
  } else if (week == "星期六") {
    var num2 = 5
  } else if (week == "星期日") {
    var num2 = 6
  }
  var num1 = getDays(strDateStart, strDateEnd);//num1代表两个日期之间相差天数
  //console.log(num1)
  var num3 = num1 + num2;//离星期1之间的相差值
  //console.log(num3)
  if (num3 % 7 == 0) {
    var n1 = Math.ceil(num3 / 7);
    return n1 + 1
  } else {
    var n1 = Math.ceil(num3 / 7);
    return n1
  }
}

//根据差值算出差值之后的日期
function getDateByNum(AddDayCount) {
  var dd = new Date();
  dd.setDate(dd.getDate() + AddDayCount); //获取AddDayCount天后的日期    
  var y = dd.getFullYear();
  var m = (dd.getMonth() + 1) < 10 ? "0" + (dd.getMonth() + 1) : (dd.getMonth() + 1); //获取当前月份的日期，不足10补0    
  var d = dd.getDate() < 10 ? "0" + dd.getDate() : dd.getDate(); //获取当前几号，不足10补0    
  return y + "-" + m + "-" + d;
}
//替换所有字符
String.prototype.replaceAll = function (s1, s2) {
  return this.replace(new RegExp(s1, "gm"), s2);
} 

/* 毫秒级倒计时 */
function count_down(that) {
  // 渲染倒计时时钟
  that.setData({
    clock: date_format_micro(total_micro_second)
  });
  if (total_micro_second <= 0) {
    that.setData({
      clock: "已经截止"
    });
    // timeout则跳出递归
    return;
  }
  setTimeout(function () {
    // 放在最后--
    total_micro_second -= 10;
    count_down(that);
  }
    , 10)
}
// 时间格式化输出，如03:25:19 86。每10ms都会调用一次
function date_format_micro(micro_second) {
  // 秒数
  var second = Math.floor(micro_second / 1000);
  // 小时位
  var hr = Math.floor(second / 3600);
  // 分钟位
  var min = fill_zero_prefix(Math.floor((second - hr * 3600) / 60));
  // 秒位
  var sec = fill_zero_prefix((second - hr * 3600 - min * 60));// equal to => var sec = second % 60;
  // 毫秒位，保留2位
  var micro_sec = fill_zero_prefix(Math.floor((micro_second % 1000) / 10));
  return hr + ":" + min + ":" + sec + " " + micro_sec;
}
// 位数不足补零
function fill_zero_prefix(num) {
  return num < 10 ? "0" + num : num
}

//对象按key排序
function objKeySort(obj) {//排序的函数
  var newkey = Object.keys(obj).sort();
  //先用Object内置类的keys方法获取要排序对象的属性名，再利用Array原型上的sort方法对获取的属性名进行排序，newkey是一个数组
  var newObj = {};//创建一个新的对象，用于存放排好序的键值对
  for (var i = 0; i < newkey.length; i++) {//遍历newkey数组
    newObj[newkey[i]] = obj[newkey[i]];//向新创建的对象中按照排好的顺序依次增加键值对
  }
  return newObj;//返回排好序的新对象
}

//判断图片地址是否存在
function validateImage(imgUrl) {
  wx.request({
    url: imgUrl, //仅为示例，并非真实的接口地址
    header: {
      'content-type': 'application/json' // 默认值
    },
    complete: function(res){
      //console.log(res)
      if(res.errMsg != ""){
        return false;
      }else{
        return true;
      }
    }
  })
}

function toBarcode (canvasId, code, width, height) {
  barcode.code128(wx.createCanvasContext(canvasId), code, width, height);
}

function contentConv(content) {
  return content.replace(/<(img).*?(\/>|<\/img>)/g, function (mats) {
      if (mats.indexOf('style') < 0) {
          return mats.replace(/<\s*img/, '<img style="max-width:100%;height:auto;"');
      } else {
          return mats.replace(/style=("|')/, 'style=$1max-width:100%;height:auto;')
      }
  });
}
//更改时间字符
function getDateTime(dataTime,type) {
  var dd = new Date(dataTime);
  var y = dd.getFullYear();
  var m = (dd.getMonth() + 1) < 10 ? "0" + (dd.getMonth() + 1) : (dd.getMonth() + 1); //获取当前月份的日期，不足10补0    
  var d = dd.getDate() <= 10 ? "0" + dd.getDate() : dd.getDate(); //获取当前几号，不足10补0  
  var h = dd.getHours() >= 10 ? dd.getHours() : '0' + dd.getHours() //时
  var M = dd.getMinutes() > 10 ? dd.getMinutes() : '0' + dd.getMinutes() //分
  if(type==1){
    return y + "-" + m + "-" + d+" "+h+":"+M;
  }else{
    return y + "年" + m + "月" + d+"日 "+h+":"+M;
  }
  
}


module.exports = {
  isEmpty: isEmpty,
  formatTime: formatTime,
  getWeek: getWeek,
  getDays: getDays,
  getNowWeeks: getNowWeeks,
  getDateByNum: getDateByNum,
  date_format_micro: date_format_micro,
  count_down: count_down,
  objKeySort: objKeySort,
  validateImage: validateImage,
  formatDateYY_MM_DD: formatDateYY_MM_DD,
  formatTimeHH_MM: formatTimeHH_MM,
  toBarcode: toBarcode,
  contentConv: contentConv,
  getDateTime: getDateTime
}