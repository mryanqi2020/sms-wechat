/**
 url 路径
 methodType 请求类型
 data 参数
 ishideload 是否隐藏loading
 callback 回调
 */
// 本地
// 管理端
var BASE_URL = "http://192.168.124.19:8021/yethan"; 
// 用户端
var BASE_STUDENT_URL = "http://192.168.124.19:9200/yethan";  



// // 网络
// var BASE_URL = "https://civiltest.ix2.com/yethan"
export const Request = (url, methodType, data, ishideload, callback) => {
    let token = wx.getStorageSync('token')||''; 
    let skey = wx.getStorageSync('skey')||''; 
    let loginType = wx.getStorageSync('loginType')||''; 
    if (!ishideload) {
        wx.showLoading({
            title: '加载中···'
        })
    }
    return new Promise((resolve, reject) => {
      // console.log(url,methodType);
        wx.request({
            url: (loginType!=='STUDENT'?BASE_URL:BASE_STUDENT_URL) + url, 
            method: methodType,
            data,
            header: {
                'content-type': 'application/json', // 默认值
                'ytoken': token,
                'grant':'WX', 
                'skey':skey 
            },
            success(res) {
                if (typeof callback === 'function') {
                    callback(res.data);
                }
                resolve(res.data || res)
                wx.hideLoading()
            },
            fail() { 
                if (typeof error === 'function') {
                    error();
                } else {
                    wx.showModal({
                        title: '访问失败',
                        content: '无法连接到服务器，请检查网络连接是否正常',
                    })
                }
                reject("访问失败")
            },
            complete: function (res) {
                wx.hideLoading();
                if (res.data.code == "00000") {
                    wx.hideLoading()
                } else {
                    // wx.showToast({
                    //     title: res.data.message,
                    //     icon: 'warn',
                    //     duration: 500
                    // })
                }
            }
        })
    })
}