//获取字典接口
import {
  Request
} from "./utils/request";

/**获取教学类型接口*/
export const getJxlx = (params) => {
  return Request("/public/dict/jylx", 'get', params);
};
// 获取校区信息
export const getcampus = (parmas) => {
  return Request("/public/dict/campus", 'get',parmas)
};
//获取预约系所
export const getBasesection = (params) => {
  return Request("/public/dict/base_section", 'get', params);
};
//获取预约会议室
export const getMeetroom = (params) => {
  return Request("/public/dict/meetRoom", 'get', params);
};
//获取申请类型
export const getsqlx = (params) => {
  return Request("/public/dict/sqlx", 'get', params);
};