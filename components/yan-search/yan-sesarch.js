// components/yan-search/yan-sesarch.js

// 使用：<yan-search bgc="#4fc08d" bind:search="onSearch"></yan-search>

Component({
  options: {
    addGlobalClass:true,
    styleIsolation: 'shared',
    multipleSlots: true // 在组件定义时的选项中启用多 slot 支持
  },
  /**
   * 组件的属性列表
   */
  properties: {
    bgc: {
      type:String,
      
      value:'#4d4dc5'
    }, // 背景颜色
    placeholder:{
      type: String,
      value: "请输入关键字搜索"
    },
  },

  /**
   * 组件的初始数据
   */
  data: {
    value:''
  },

  /**
   * 组件的方法列表
   */
  methods: {

    // 	输入内容变化时触发 
    onChange(e) {
      // 将内容于data.value绑定
      this.setData({
        value: e.detail,
      });
      console.log(3)
    },
    // 确定搜索时触发
    onSearch(e){
      const detail = e.detail; // 得到搜索关键字
      this.triggerEvent('search', detail)
    },
    onClick(){
      console.log(2)
      this.triggerEvent('search', this.data.value)
    }

  }
})
