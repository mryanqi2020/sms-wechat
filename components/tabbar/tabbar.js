// components/tabbar/tabbar.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    userRole: String,
  },

  /**
   * 组件的初始数据
   */
  data: {
    active: 0,
    tabbarList: [],
    tabbarMap: {
      student: [{
          "iconPath": "/images/icon/home1.png",
          "selectedIconPath": "/images/icon/home2.png",
          "pagePath": "/pages/index/index",
          "text": "首页"
        },
        {
          "iconPath": "/images/icon/grade1.png",
          "selectedIconPath": "/images/icon/grade2.png",
          "pagePath": "/pages/student/grade/grade",
          "text": "班级"
        },
        {
          "iconPath": "/images/icon/attendance1.png",
          "selectedIconPath": "/images/icon/attendance2.png",
          "pagePath": "/pages/student/attendance/attendance",
          "text": "签到"
        },
        {
          "iconPath": "/images/icon/news1.png",
          "selectedIconPath": "/images/icon/news2.png",
          "pagePath": "/pages/student/news/news",
          "text": "消息"
        },
        {
          "iconPath": "/images/icon/about1.png",
          "selectedIconPath": "/images/icon/about2.png",
          "pagePath": "/pages/student/about/about",
          "text": "我的"
        }
      ],
      counselor: [{
          "iconPath": "/images/icon/home1.png",
          "selectedIconPath": "/images/icon/home2.png",
          "pagePath": "/pages/index/index",
          "text": "首页"
        },
        {
          "iconPath": "/images/icon/grade1.png",
          "selectedIconPath": "/images/icon/grade2.png",
          "pagePath": "/pages/teacher/grade/grade",
          "text": "班级"
        },
        {
          "iconPath": "/images/icon/about1.png",
          "selectedIconPath": "/images/icon/about2.png",
          "pagePath": "/pages/student/about/about",
          "text": "我的"
        }
      ]
    }
  },
  pageLifetimes: {
    show: function () {
      // 页面被展示
      // 在需要获取路由信息的地方调用该方法
      const pages = getCurrentPages(); // 获取当前页面栈
      const currentPage = pages[pages.length - 1]; // 获取当前页面实例
      const route = '/' + currentPage.route; // 获取当前页面的路由路径
      console.log(this.data.userRole);
      this.setData({
        tabbarList: this.data.tabbarMap[this.data.userRole],
      })
      this.setData({
        active:this.data.tabbarList.findIndex(d=>{
          return d.pagePath === route;
        })
      })
    },
  },
  /**
   * 组件的方法列表
   */
  methods: {
    // 跳转
    onChange(e) {
      const curIdx = e.detail;
      const pagePath = this.data.tabbarList.find((d, idx) => idx === curIdx).pagePath;
      wx.reLaunch({
        url: pagePath,
      })
    }
  }
})